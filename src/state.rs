use crate::constants::{WINDOW_SIZE_X, WINDOW_SIZE_Y};
use crate::{game::Game, vector::Vector2, Direction};
use piston_window::types::Color;
use piston_window::*;

const BACK_COLOR: Color = [0.204, 0.286, 0.369, 1.0];

pub struct GameState {
    fps: u64,
    game: Game,
    window: PistonWindow,
}

impl GameState {
    pub fn new(settings: WindowSettings) -> Self {
        GameState {
            fps: 0,
            game: Game::new(Vector2::default(), WINDOW_SIZE_X as i32, WINDOW_SIZE_Y as i32),
            window: settings
                .build()
                .unwrap_or_else(|e| panic!("Failed to build PistonWindow: {}", e)),
        }
    }

    fn update_on_tick(&mut self, new_dir: Option<Direction>) {
        self.game.update(new_dir);
    }

    pub fn cap_fps(&mut self, target: u64) {
        self.window.set_max_fps(target);
    }

    pub fn get_event(&mut self) -> Option<Event> {
        self.window.next()
    }

    pub fn key_pressed(&mut self, key: keyboard::Key) {
        // update the snake's head direction
        let dir = match key {
            Key::W => Some(Direction::Up),
            Key::S => Some(Direction::Down),
            Key::A => Some(Direction::Left),
            Key::D => Some(Direction::Right),
            _ => None,
        };

        if let Some(dir) = dir {
            if dir == self.game.snake.face_direction().opposite() {
                return;
            }
            self.update_on_tick(Some(dir));
        }
    }

    pub fn render(&mut self, event: &Event) {
        self.window.draw_2d(event, |c, g, _d| {
            clear(BACK_COLOR, g);
            self.game.draw(&c, g);
        });
    }

    pub fn get_fps(&self) -> u64 {
        self.fps
    }

    pub fn update_fps(&mut self, fps: u64) {
        self.fps = fps;
    }

    pub fn tick(&mut self, dt: f64) {
        self.game.tick(dt);
        self.update_on_tick(None);
    }
}
