mod constants;
mod direction;
mod draw;
mod game;
mod logger;
mod macros;
mod snake;
mod state;
mod vector;

use backtrace::Backtrace;
use constants::*;
use direction::*;
use lazy_static::lazy_static;
use log::{LevelFilter, SetLoggerError};
use logger::GameLogger;
use piston_window::*;
use rand::Rng;
use snake::*;
use state::*;
use std::{env, panic};
use vector::*;

lazy_static! {
    static ref GAME_WINDOW_SETTINGS: WindowSettings =
        WindowSettings::new("Hello", (WINDOW_SIZE_X, WINDOW_SIZE_Y))
            .exit_on_esc(true)
            .resizable(false);
}

static MAX_SNAKE_SPEED: f64 = TICKS_PER_SECOND;
static MIN_SNAKE_SPEED: f64 = 1.0 / 1.5;

fn set_panic_hook() {
    // copied straight from: https://github.com/rojo-rbx/remodel/blob/master/src/main.rs
    // it is licensed under MIT License: https://github.com/rojo-rbx/remodel/blob/master/LICENSE.txt
    panic::set_hook(Box::new(|panic_info| {
        let message = match panic_info.payload().downcast_ref::<&str>() {
            Some(message) => message.to_string(),
            None => match panic_info.payload().downcast_ref::<String>() {
                Some(message) => message.clone(),
                None => "<no message>".to_string(),
            },
        };

        log::error!("Game crashed!");
        log::error!("This may be a bug.");
        log::error!("");
        log::error!("Please report to this to the developer or who made the game.");
        log::error!("Details: {}", message);

        if let Some(location) = panic_info.location() {
            log::error!("in file {} on line {}", location.file(), location.line());
        }

        // When using the backtrace crate, we need to check the RUST_BACKTRACE
        // environment variable ourselves. Once we switch to the (currently
        // unstable) std::backtrace module, we won't need to do this anymore.
        let should_backtrace = env::var("RUST_BACKTRACE")
            .map(|var| var == "1")
            .unwrap_or(false);

        if should_backtrace {
            eprintln!("{:?}", Backtrace::new());
        } else {
            eprintln!(
                "note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace."
            );
        }

		#[allow(clippy::empty_loop)]
		loop {}
    }));
}

fn initialize_logger() -> Result<(), SetLoggerError> {
    log::set_logger(&GameLogger)?;
    // production release
    #[cfg(not(debug_assertions))]
    {
        println!("{}", INSTRUCTIONS);
        log::set_max_level(LevelFilter::Warn);
    }
    #[cfg(debug_assertions)]
    {
        log::set_max_level(LevelFilter::Trace);
    }
    set_panic_hook();
    Ok(())
}

fn main() {
    // @TODO: Dangerously initializes logger, which is not good
    // for users who doesn't know how to code in Rust.
    initialize_logger().expect("Failed to initialize logger");

    // initialize piston window
    log::info!("Initializing window");

	let mut rng = rand::thread_rng();
    let mut state = GameState::new(GAME_WINDOW_SETTINGS.clone());
    state.cap_fps(60);

    log::info!("Running game");

    let mut tick_timer: f64 = 0.0;
    let mut fps_timer: f64 = 0.0;

    while let Some(e) = state.get_event() {
        // input goes first, before the game then render
        if let Some(Button::Keyboard(key)) = e.press_args() {
            state.key_pressed(key);
        }

        e.update(|arg| {
            tick_timer += arg.dt;

            #[cfg(debug_assertions)]
            {
                fps_timer += arg.dt;
            }

            let final_fps = if arg.dt == 0.0 {
                0
            } else {
                (1.0 / arg.dt) as u64
            };
            state.update_fps(final_fps);

            #[cfg(debug_assertions)]
            {
                if fps_timer >= FPS_DIAG_INTERVAL {
                    fps_timer = 0.0;
                    log::debug!("{} FPS", state.get_fps());
                }
            }

			let fake_ticker = rng.gen_range(MAX_SNAKE_SPEED..MIN_SNAKE_SPEED);
            if tick_timer >= fake_ticker {
                state.tick(arg.dt);
                tick_timer = 0.0;
            }
        });

        state.render(&e);
    }
}
