use crate::stop_game;

#[derive(Default, Debug, Clone, Copy, PartialEq, PartialOrd, Eq)]
pub struct Vector2 {
    pub x: i32,
    pub y: i32,
}

impl std::fmt::Display for Vector2 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Vector2({}, {})", self.x, self.y)
    }
}

#[allow(dead_code)]
impl Vector2 {
    pub fn mul_int(&self, other: i32) -> Vector2 {
        Vector2 {
            x: self.x * other,
            y: self.y * other,
        }
    }

    pub fn add_int(&self, ix: i32, iy: i32) -> Vector2 {
        Vector2 {
            x: self.x + ix,
            y: self.y + iy,
        }
    }

    pub fn add(&self, other: Vector2) -> Vector2 {
        Vector2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }

    pub fn mul(&self, other: Vector2) -> Vector2 {
        Vector2 {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }

    pub fn sub(&self, other: Vector2) -> Vector2 {
        Vector2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }

    pub fn div_int(&self, divisor: i32) -> Vector2 {
        Vector2 {
            x: self.x / divisor,
            y: self.y / divisor,
        }
    }

    pub fn clamp_and_reset(&self, max: Vector2) -> Vector2 {
        let x = if self.x > max.x + 1 || self.x < -1 {
            stop_game!()
        } else {
            self.x
        };
        let y = if self.y > max.y + 1 || self.y < -1 {
            stop_game!()
        } else {
            self.y
        };
        Vector2 { x, y }
    }

    pub fn clamp(&self, min: Vector2, max: Vector2) -> Vector2 {
        self.min(min).max(max)
    }

    pub fn is_min(&self, min: Vector2) -> bool {
        if self.x < min.x {
            return true;
        };
        if self.y < min.y {
            return true;
        };
        false
    }

    pub fn is_max(&self, max: Vector2) -> bool {
        if self.x > max.x {
            return true;
        };
        if self.y > max.y {
            return true;
        };
        false
    }

    pub fn min(&self, min: Vector2) -> Vector2 {
        let x = if self.x < min.x { min.x } else { self.x };
        let y = if self.y < min.y { min.y } else { self.y };
        Vector2 { x, y }
    }

    pub fn max(&self, max: Vector2) -> Vector2 {
        let x = if self.x > max.x { max.x } else { self.x };
        let y = if self.y > max.y { max.y } else { self.y };
        Vector2 { x, y }
    }

    pub fn is_between(&self, min: Vector2, max: Vector2) -> bool {
        min.x >= self.x && self.x <= max.x && min.y >= self.y && self.y <= max.y
    }
}
