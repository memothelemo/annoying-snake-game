use crate::constants::*;
use crate::{draw, Direction, Vector2};
use std::collections::VecDeque;

pub struct Snake {
    face: Direction,
    body: VecDeque<Vector2>,
    base: Vector2,
    size: Vector2,
}

impl Snake {
    pub fn new(spawn: Vector2, base: Vector2, size: Vector2) -> Self {
        Snake {
            face: Direction::Right,
            body: {
                let mut deque = VecDeque::new();
                deque.push_back(spawn.add_int(2, 0));
                deque.push_back(spawn.add_int(1, 0));
                deque.push_back(spawn);
                deque
            },
            base,
            size,
        }
    }

    fn clamp_in_bounds(&self, pos: Vector2) -> Vector2 {
        pos.clamp_and_reset(self.size)
    }

    pub fn predict_position(&self, dir: Option<Direction>) -> Vector2 {
        let dir = dir.unwrap_or_else(|| self.face.clone());
        let head_block = self.head();

        match dir {
            Direction::Up => self.clamp_in_bounds(Vector2 {
                x: head_block.x,
                y: head_block.y - 1,
            }),
            Direction::Down => self.clamp_in_bounds(Vector2 {
                x: head_block.x,
                y: head_block.y + 1,
            }),
            Direction::Left => self.clamp_in_bounds(Vector2 {
                x: head_block.x - 1,
                y: head_block.y,
            }),
            Direction::Right => self.clamp_in_bounds(Vector2 {
                x: head_block.x + 1,
                y: head_block.y,
            }),
        }
    }

    pub fn body(&self) -> &VecDeque<Vector2> {
        &self.body
    }

	fn fix_body(&mut self) {
		// very expensive calculation but who cares?
		let mut deque = VecDeque::new();
		for block in self.body.iter() {
			let new = self.clamp_in_bounds(*block);
			deque.push_back(new);
		}
		self.body = deque;
	}

    pub fn grow_snake(&mut self, dir: Option<Direction>) {
        self.move_in_direction(dir.clone());
        self.body.push_front(self.predict_position(dir));
		self.fix_body();
    }

    pub fn move_in_direction(&mut self, dir: Option<Direction>) {
        if let Some(d) = dir {
            self.face = d
        };

        self.body.push_front(self.predict_position(None));
        self.body.pop_back().unwrap();
		self.fix_body();
    }

    pub fn face_direction(&self) -> Direction {
        self.face.clone()
    }

    pub fn head(&self) -> Vector2 {
        *self.body.front().unwrap()
    }

    pub fn draw(&self, ctx: &piston_window::Context, g: &mut piston_window::G2d) {
        for vector in self.body.iter() {
            draw::draw_rectangle(
                self.base.add(vector.mul(*SNAKE_BLOCK_SIZE)),
                *SNAKE_BLOCK_SIZE,
                [0.5, 0.0, 0.0, 1.0],
                ctx,
                g,
            );
        }
    }
}
