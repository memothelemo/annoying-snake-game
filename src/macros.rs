#[macro_export]
macro_rules! stop_game {
	() => {
		{
			panic!("Game over! Yeah, that's how it does. Don't bother reading this, if you lose. Go to the up if you want to see the score.");
		}
	};
}
