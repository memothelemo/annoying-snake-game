use crate::vector::Vector2;

pub fn draw_rectangle(
    position: Vector2,
    size: Vector2,
    color: [f32; 4],
    c: &piston_window::Context,
    g: &mut piston_window::G2d,
) {
    piston_window::rectangle(
        color,
        [
            position.x as f64,
            position.y as f64,
            size.x as f64,
            size.y as f64,
        ],
        c.transform,
        g,
    );
}
