use crate::constants::*;
use crate::*;

use rand::prelude::ThreadRng;
use rand::{thread_rng, Rng};

static BAD_FOOD_COLOR: [f32; 4] = [0.1, 0.6, 0.1, 1.0];
static GOOD_FOOD_COLOR: [f32; 4] = [0.6, 0.6, 0.1, 1.0];

pub struct Food {
    pub location: Vector2,
    pub is_evil: bool,
    pub rng: ThreadRng,
    pub map_base: Vector2,
    pub map_limits: Vector2,
}

impl Food {
    pub fn new(map_base: Vector2, rng: ThreadRng, limits: Vector2) -> Self {
        let mut food = Food {
            location: Vector2::default(),
            is_evil: false,
            rng,
            map_base,
            map_limits: limits,
        };
        food.relocate();
        food
    }

    pub fn relocate(&mut self) {
        let x = self.rng.gen_range(0..self.map_limits.x);
        let y = self.rng.gen_range(0..self.map_limits.y);
        self.is_evil = rand::random::<bool>();
        self.location = Vector2 { x, y };
        log::debug!("Relocating food to {}", self.location);
    }

    pub fn draw(&self, ctx: &piston_window::Context, g: &mut piston_window::G2d) {
        let color = match self.is_evil {
            true => BAD_FOOD_COLOR,
            false => GOOD_FOOD_COLOR,
        };
        draw::draw_rectangle(
            self.map_base.add(self.location.mul(*FOOD_BLOCK_SIZE)),
            *FOOD_BLOCK_SIZE,
            color,
            ctx,
            g,
        );
    }
}

pub struct Game {
    pub base: Vector2,
    pub food: Food,
    pub rng: ThreadRng,
	pub max_time: f64,
    pub snake: Snake,
	pub score: u32,
    pub timer: f64,
}

impl Game {
    pub fn new(base: Vector2, width: i32, height: i32) -> Self {
        let width = width / SNAKE_BLOCK_SIZE.x;
        let height = height / SNAKE_BLOCK_SIZE.y;
        let end = base.add_int(width, height);
        let limits = Vector2 {
            x: width,
            y: height,
        };
        Game {
            base,
            food: Food::new(base, thread_rng(), limits),
            rng: thread_rng(),
			max_time: 2.0,
            snake: Snake::new(end.sub(base).div_int(2), base, limits),
			score: 0,
            timer: 0.0,
        }
    }

	fn eat_food(&mut self) {
		self.score += 1;
		self.food.relocate();
		self.snake.grow_snake(None);

		log::debug!("The snake ate the food");
		println!("Score: {}", self.score);
	}

    pub fn tick(&mut self, dt: f64) {
        self.timer += dt;
    }

    pub fn update(&mut self, dir: Option<Direction>) {
		if self.timer >= self.max_time {
            self.timer = 0.0;
			self.max_time = self.rng.gen_range(2.0..3.0);
            self.food.relocate();

            // chance a snake will grow?
            if rand::random::<bool>() {
                self.snake.grow_snake(None);
            }
        }

        self.snake.move_in_direction(dir);

        let mut relocate_food = false;
        let next_pos = self.snake.predict_position(None);
        let food_loc = self.food.location;

        for block in self.snake.body().iter() {
            if food_loc.eq(block) {
                if self.food.is_evil {
                    stop_game!()
                } else {
                    relocate_food = true;
					break;
                }
            }
            if next_pos.eq(block) {
                stop_game!()
            }
        }

        if relocate_food {
            self.eat_food();
        }
    }

    pub fn draw(&self, ctx: &piston_window::Context, g: &mut piston_window::G2d) {
        self.snake.draw(ctx, g);
        self.food.draw(ctx, g);
    }
}
