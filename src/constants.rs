#[allow(dead_code)]
pub static INSTRUCTIONS: &str = include_str!("inst.txt");

pub static WINDOW_SIZE_X: u32 = 600;
pub static WINDOW_SIZE_Y: u32 = 600;

pub static TICKS_PER_SECOND: f64 = 1.0 / 30.0;

pub static FPS_DIAG_INTERVAL: f64 = 3.0;

use crate::vector::Vector2;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref SNAKE_BLOCK_SIZE: Vector2 = Vector2 { x: 40, y: 40 };
    pub static ref FOOD_BLOCK_SIZE: Vector2 = Vector2 { x: 40, y: 40 };
}
